import {
  initAction,
  addColAction,
  addDetAction,
  deleteColAction,
  deleteDetAction,
} from "./actions";
import { CollectionUrls } from "./urls";
import axios from "axios";

export const initCollections = (token) => {
  const collectionUrl = CollectionUrls.COLLECTION;
  return async (dispatch) => {
    const response = await axios.get(collectionUrl, {
      headers: { Authorization: "Token " + token },
    });
    dispatch(initAction(response.data));
  };
};

export const handleDelete = async (token, targetId, n, list) => {
  let url = CollectionUrls.COLLECTION;
  let action = () => {};
  if (!targetId.item) {
    //呼び出し元がCollectionCard
    url = url + targetId;
    action = deleteColAction;
  } else {
    // 呼び出し元がBookCard
    url = url + targetId.col + "/books/" + targetId.item;
    action = deleteDetAction;
  }

  return async (dispatch) => {
    console.log("fetch");
    const response = await axios.get(url, {
      headers: { Authorization: "Token " + token },
    });
    console.log(response);
    list.splice(n, 1);
    dispatch(action(list));
  };
};

export const handleAdd_Collection = (token, collection, books) => {
  const url = CollectionUrls.COLLECTION + collection.id + "/books/";
  const data = { id: collection.id, book: books };
  const list = collection.books;
  return async (dispatch) => {
    if (!token) {
      dispatch.push("/login");
    }
    const response = await axios.post(url, data, {
      headers: { Authorization: "Token " + token },
    });
    list.push(response.data);
    dispatch(addDetAction(list));
  };
};

export const handleAdd_Search = (token, book, col, results, collectionName) => {
  let url = CollectionUrls.COLLECTION;
  let action = () => {};
  let data = {};
  let list = [];

  if (col !== "new") {
    action = addDetAction;
    url = url + col.id + "/books/";
    data = { id: col.id, book: book };
    list = col.books;
  } else {
    action = addColAction;
    data = { name: collectionName, books: Array(1).fill({ book: book }) };
    list = results;
  }
  return async (dispatch) => {
    if (!token) {
      dispatch.push("/login");
    }
    const response = await axios.post(url, data, {
      headers: { Authorization: "Token " + token },
    });
    list.push(response.data);
    dispatch(action(list));
  };
};

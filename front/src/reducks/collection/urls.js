const ROOT_URL = "http://localhost:8000/";

export const CollectionUrls = {
  SEARCH: `${ROOT_URL}api/collection/search/`,
  COLLECTION: `${ROOT_URL}api/collection/collection/`,
};

import axios from "axios";
import { AuthUrls } from "./urls";

export const signup = (values) => {
  const signupUrl = AuthUrls.SIGNUP;
  return axios.post(signupUrl, values);
};

export const verify = (data) => {
  const activateUserUrl = AuthUrls.USER_ACTIVATION;
  return axios.post(activateUserUrl, data)
}

export const login = (values) => {
  const loginUrl = AuthUrls.LOGIN;
  return axios.post(loginUrl, values);
};

export const logout = () => {
  const logoutUrl = AuthUrls.LOGOUT;
  return axios.post(logoutUrl);
};

// function processServerError(error) {
//   return  Object.keys(error).reduce(function(newDict, key) {
//       if (key === "non_field_errors") {
//           newDict["_error"].push(error[key]);
//       } else if (key === "token") {
//           // token sent with request is invalid
//           newDict["_error"].push("The link is not valid any more.");
//       } else {
//           newDict[key] = error[key];
//       }

//       return newDict
//   }, {"_error": []});
// }

import { Record } from "immutable"

export const User = Record({
  done: false,
  token: sessionStorage.getItem("token") | null,
})

export class UserModel extends User {
  getToken(){
    return this.get('token')
  }
  setToken(token){
    sessionStorage.setItem({"token": token})
    return this.set('token', token)
  }
  removeToken(){
    sessionStorage.removeItem("token")
    return this.set('token', null)
  }
  toggleDone(){
    return this.set('done', !this.done)
  }
}
import {User} from './models'
import * as Actions from "./actions";

export const authReducer = (model = new User, action) => {
  switch (action.type) {
    case Actions.SIGNUP_DONE:
      return model.toggleDone();
    case Actions.LOGIN_SUCCESS:
      return model.setToken(action.payload);
    case Actions.LOGIN_ERROR:
      return ;
    case Actions.LOGOUT_SUCCESS:
      return model.removeToken();
    case Actions.LOGOUT_ERROR:
      return ;
    case Actions.VERIFICATION_SUCCESS:
      return model.toggleDone();

    default:
      return model;
  }
};

import { put, call, take } from "redux-saga/effects";
import * as Api from "./apis";
import * as Actions from "./actions";

export function* handleSignupRequest() {
  const action = yield take(Actions.SIGNUP_REQUEST);
  const response = yield call(Api.signup, action.payload.values); //formの入力値を渡す
  if (response) {
    yield put(Actions.signupDone(response.data.detail));
  } else {
    yield put(Actions.signupError("登録に失敗しました。"));
  }
}

export function* handleLoginRequest() {
  const action = yield take(Actions.LOGIN_REQUEST);
  const response = yield call(Api.login, action.payload.values); //formの入力値を渡す
  if (response) {
    yield put(Actions.loginSuccess(response.data.key));
  } else {
    yield put(Actions.loginError("ログインに失敗しました。"));
  }
}

export function* handleLogoutRequest() {
  yield take(Actions.LOGOUT_REQUEST);
  const response = yield call(Api.logout);
  if (response) {
    yield put(Actions.logoutSuccess());
  } else {
    yield put(Actions.logoutError("ログアウトに失敗しました。"));
  }
}

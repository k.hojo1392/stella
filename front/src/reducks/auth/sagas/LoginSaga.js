import { call, put, take } from "redux-saga/effects";
import * as Api from "../apis";
import * as Actions from "../actions";

export class LoginSaga {
  constructor(action) {
    this.action = action;
  }
  *request() {
    const response = yield call(Api.login, this.action.payload.values); //formの入力値を渡す
    if (response) {
      yield put(Actions.loginSuccess(response.data.key));
    } else {
      yield put(Actions.loginError("ログインに失敗しました。"));
    }
  }
}

export function* login() {
  const action = yield take(Actions.LOGIN_REQUEST);
  const instance = new LoginSaga(action);
  yield call([instance, instance.request]);
}

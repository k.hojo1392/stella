import { call, put, take } from "redux-saga/effects";
import * as Api from "../apis";
import * as Actions from "../actions";

export class LogoutSaga {
  constructor(action) {
    this.action = action;
  }
  *request() {
    const response = yield call(Api.logout);
    if (response) {
      yield put(Actions.logoutSuccess());
    } else {
      yield put(Actions.logoutError("ログアウトに失敗しました。"));
    }
  }
}

export function* logout() {
  const action = yield take(Actions.LOGOUT_REQUEST);
  const instance = new LogoutSaga(action);
  yield call([instance,instance.request()]);
}

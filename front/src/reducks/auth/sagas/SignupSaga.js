import { put, call, take } from "redux-saga/effects";
import * as Api from "../apis";
import * as Actions from "../actions";

class SignupSaga {
  constructor(action) {
    this.action = action;
  }

  *request() {
    const response = yield call(Api.signup, this.action.payload.values); //formの入力値を渡す
    if (response) {
      yield put(Actions.signupDone());
    } else {
      yield put(Actions.signupError("登録に失敗しました。"));
    }
  }
}

export function* signup() {
  const action = yield take(Actions.SIGNUP_REQUEST);
  const instance = new SignupSaga(action);
  yield call([instance, instance.request()]);
}

import { testSaga, expectSaga } from "redux-saga-test-plan";
import { call } from "@redux-saga/core/effects";
import { login, LoginSaga } from "./LoginSaga";
import * as Actions from '../actions'

describe.skip('tests login function', () => {
  it('should yield take', () => {
    const saga = testSaga(login)
    saga.next().take(Actions.LOGIN_REQUEST)
    saga.next().isDone()
  })
  it('should take login request', () => {
    const values = {}
    return expectSaga(login)
      .take(Actions.LOGIN_REQUEST)
      .dispatch(Actions.loginRequest(values))
      .run()
  })
  // it('should call request method', () => {
  //   const
  // })
})

describe.skip('tests LoginSaga class', () => {
  const action = Actions.loginRequest
  const api = {login:() => ''}
  const instance = new LoginSaga(action)
  describe.skip('tests request method', () => {
    it('should put loginSuccess with token', () => {
      const key = 'some string contained in response'
      return expectSaga(instance.request)
        .provide([[call(api.login), key]])
        .put(Actions.loginSuccess(key))
        .run()
    })
  })
})
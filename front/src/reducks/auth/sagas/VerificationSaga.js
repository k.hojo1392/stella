import { put, call, take } from 'redux-saga'
import * as Api from '../apis'
import * as Actions from '../actions'

class VerificationSaga {
  constructor(action){
    this.action = action
  }
  *request(){
    const response = yield call(Api.verify, this.action.payload)
    if (response) {
      yield put(Actions.verificationSuccess());
    } else {
      yield put(Actions.verificationError());
    }
  }
}

export function* verification() {
  const action = yield take(Actions.VERIFICATION_REQUEST);
  console.log('take')
  const instance = new VerificationSaga(action);
  yield call([instance, instance.request()]);
}


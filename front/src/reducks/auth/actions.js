export const SIGNUP_REQUEST = "SIGNUP_REQUEST";
export const signupRequest = (formValues) => {
  console.log('requested')
  return {
    type: "SIGNUP_REQUEST",
    payload: {
      formValues,
    },
  };
};

export const SIGNUP_DONE = "SIGNUP_DONE";
export const signupDone = () => {
  return {
    type: "SIGNUP_DONE",
    payload: {
      done: true,
    },
  };
};

export const SIGNUP_ERROR = "SIGNUP_ERROR";
export const signupError = (error) => {
  return {
    type: "SIGNUP_ERROR",
    payload: {
      error: error,
    },
  };
};

export const VERIFICATION_REQUEST = 'VERIFICATION_REQUEST';
export const verificationRequest = (key) => {
  return {
    type: "VERIFICATION_REQUEST",
    payload: {
      key: key
    }
  }
}

export const VERIFICATION_SUCCESS = 'VERIFICATION_SUCCESS'
export const verificationSuccess = () => {
  return {
    type: "VERIFICATION_SUCCESS",
    payload: {
      done: false,
    }
  }
}

export const VERIFICATION_ERROR = 'VERIFICATION_ERROR'
export const verificationError = () => {
  return {
    type: "VERIFICATION_ERROR",
    payload: {
      verified: false,
    }
  }
}

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const loginRequest = (formValues) => {
  console.log('requested')
  return {
    type: "LOGIN_REQUEST",
    payload: {
      values: formValues,
    },
  };
};

export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const loginSuccess = (token) => {
  return {
    type: "LOGIN_SUCCESS",
    payload: {
      done: "",
      token: token,
    },
  };
};

export const LOGIN_ERROR = "LOGIN_ERROR";
export const loginError = (error) => {
  return {
    type: "LOGIN_ERROR",
    payload: {
      error: error,
    },
  };
};

export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const logoutRequest = () => {
  return {
    type: "LOGOUT_REQUEST",
    payload: {},
  };
};

export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const logoutSuccess = () => {
  return {
    type: "LOGOUT_SUCCESS",
    payload: {
      done: "",
      token: "",
    },
  };
};

export const LOGOUT_ERROR = "LOGOUT_ERROR";
export const logoutError = (error) => {
  return {
    type: "LOGOUT_ERROR",
    payload: {
      error: error,
    },
  };
};

import { fork } from "redux-saga/effects";
import { login } from "../auth/sagas/LoginSaga";
import { logout } from "../auth/sagas/LogoutSaga";
import { signup } from "../auth/sagas/SignupSaga";
import { search,page } from "../search/sagas/SearchSaga";

export default function* rootSaga() {
  yield fork(signup);
  yield fork(login);
  yield fork(logout);
  yield fork(search);
  yield fork(page);
}

import {Record} from 'immutable'

const initialState = Record({
  auth: {
    done: "",
    token: sessionStorage.getItem("token"),
  },
  search: {
    result: [],
    number: "",
    page: 1,
  },
  collection: {
    data: { count: 0, results: [{ name: "" }] },
  },
  detail: {
    detail: {},
  },
});

export default initialState;

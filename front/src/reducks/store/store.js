import {
  createStore as reduxCreateStore,
  applyMiddleware,
  compose,
} from "redux";
import { routerMiddleware } from "connected-react-router";
import createRootReducer from "./rootReducer";
// import thunk from 'redux-thunk';
import createSagaMiddleware from "redux-saga";
// import initialState from "./initialState";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const sagaMiddleware = createSagaMiddleware();

export function createStore(history) {
  return reduxCreateStore(
    createRootReducer(history),
    // initialState,
    composeEnhancers(
      applyMiddleware(
        routerMiddleware(history),
        // thunk
        sagaMiddleware
      )
    )
  );
}

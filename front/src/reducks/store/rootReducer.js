import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { authReducer } from "../auth/reducers";
import { reducer as formReducer } from "redux-form";
import { searchReducer } from "../search/reducers";
import { collectionReducer, detailReducer } from "../collection/reducers";

const createRootReducer = (history) => {
  return combineReducers({
    router: connectRouter(history),
    form: formReducer,
    auth: authReducer,
    search: searchReducer,
    collection: collectionReducer,
    detail: detailReducer,
  });
};

export default createRootReducer;

import { testSaga } from "redux-saga-test-plan";
import { login } from "../auth/sagas/LoginSaga";
import { logout } from "../auth/sagas/LogoutSaga";
import { signup } from "../auth/sagas/SignupSaga";
import { search, page } from "../search/sagas";
import rootSaga from "./rootSaga";

describe.skip("tests rootSaga", () => {
  it("should yield four forks", () => {
    const saga = testSaga(rootSaga);
    saga.next().fork(signup);
    saga.next().fork(login);
    saga.next().fork(logout);
    saga.next().fork(search);
    saga.next().fork(page);
    saga.next().isDone();
  });
});

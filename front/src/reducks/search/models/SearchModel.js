import { Record, List, fromJS } from "immutable";

const SearchResult = Record({
  number: '',
  result: List(),
  page: 1,
});

export class SearchModel extends SearchResult {
  constructor(args) {
    super(args);
  }
  setResult(data) {
    console.log(fromJS(data.result))
    const result = this.withMutations((s) =>
      s.set("number", data.number).set("result", fromJS(data.result))
    );
    console.log(result)
    return result
  }
  setPage(page) {
    return this.set("page", page);
  }
}

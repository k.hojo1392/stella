import { Record } from "immutable";

const BookRecord = Record({
  title: null,
  creator: null,
  published: null,
  issued: null
})

export class BookModel extends BookRecord {
  
}
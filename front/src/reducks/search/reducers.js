import { SearchModel } from "./models/SearchModel";
import * as Actions from "./actions";

export const searchReducer = (model = new SearchModel, action) => {
    switch (action.type) {
      case Actions.SEARCH_SUCCESS:
        return model.setResult(action.payload);
      case Actions.CHANGE_PAGE_REQUEST:
        return model.setPage(action.payload.page);
      default:
        return model;
    }
  }
;

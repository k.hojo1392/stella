import axios from "axios";
import { CollectionUrls } from "../collection/urls";

export const search = (params) => {
  const searchUrl =
    CollectionUrls.SEARCH +
    "?title=" +
    encodeURIComponent(params.values.title) +
    "&ndc=" +
    params.values.ndc +
    "&page=" +
    params.page;

  return axios.get(searchUrl);
};

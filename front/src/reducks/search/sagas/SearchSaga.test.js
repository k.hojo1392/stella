import { testSaga, expectSaga } from "redux-saga-test-plan";
import { call } from "@redux-saga/core/effects";
import { search, SearchSaga } from "./SearchSaga";
import * as Actions from '../actions'

describe('tests search function', () => {
  it('should yield take', () => {
    const saga = testSaga(search)
    saga.next().take(Actions.SEARCH_REQUEST)
    saga.next().isDone()
  })
  it('should take search request', () => {
    const values = {}
    const page = 1
    return expectSaga(search)
      .take(Actions.SEARCH_REQUEST)
      .dispatch(Actions.searchRequest(values, page))
      .run()
  })
  // it('should call request method', () => {
  //   const
  // })
})

describe.skip('tests SearchSaga class', () => {
  const action = Actions.searchRequest
  const api = {search:() => ''}
  const instance = new SearchSaga(action)
  describe.skip('tests request method', () => {
    it('should put searchSuccess', () => {
      const key = 'some string contained in response'
      return expectSaga(instance.request)
        .provide([[call(api.search), key]])
        .put(Actions.searchSuccess(key))
        .run()
    })
  })
})
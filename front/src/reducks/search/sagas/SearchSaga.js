import { call, put, take } from "redux-saga/effects";
import * as Actions from "../actions";
import * as Api from "../apis";

export class SearchSaga {
  constructor(action){
    this.action = action
  }
  *request(){
    const response = yield call(Api.search, this.action.payload)
    if (response) {
      yield put(Actions.searchSuccess(response.data))
    }
  }
}

export function* search() {
  const action = yield take(Actions.SEARCH_REQUEST)
  const instance = new SearchSaga(action)
  yield call([instance, instance.request])
}

export function* page(){
  const action = yield take(Actions.CHANGE_PAGE_REQUEST)
  const instance = new SearchSaga(action)
  yield call([instance, instance.request])
}
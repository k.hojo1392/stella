// import axios from "axios";
// import { searchAction, pageAction } from "./actions";

// import { CollectionUrls } from "../collection/urls";

// export const search = (values) => {
//   return async (dispatch) => {
//     const searchUrl =
//       CollectionUrls.SEARCH +
//       "?title=" +
//       encodeURIComponent(values.title) +
//       "&ndc=" +
//       values.ndc;

//     const response = await axios.get(searchUrl);
//     dispatch(
//       searchAction({
//         number: response.data.numberOfRecords,
//         result: response.data.result,
//       })
//     );
//     dispatch(pageAction(1));
//   };
// };

// export const changePage = (page, values) => {
//   return async (dispatch) => {
//     const searchUrl =
//       CollectionUrls.SEARCH +
//       "?title=" +
//       encodeURIComponent(values.title) +
//       "&ndc=" +
//       values.ndc +
//       "&page=" +
//       page;
//     const response = await axios.get(searchUrl);
//     dispatch(
//       searchAction({
//         number: response.data.numberOfRecords,
//         result: response.data.result,
//       })
//     );
//     dispatch(pageAction(page));
//   };
// };

export const SEARCH_REQUEST = "SEARCH_REQUEST";
export const searchRequest = (values) => {
  return {
    type: "SEARCH_REQUEST",
    payload: {
      values,
      page: 1
    }
  };
};

export const SEARCH_SUCCESS = "SEARCH_SUCCESS";
export const searchSuccess = (data) => {
  return {
    type: "SEARCH_SUCCESS",
    payload: data
  };
};

export const CHANGE_PAGE_REQUEST = "CHANGE_PAGE_REQUEST";
export const changePageRequest = (values, page) => {
  return {
    type: "CHANGE_PAGE_REQUEST",
    payload: {
      values,
      page
    }
  };
};

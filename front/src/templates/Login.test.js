import { jssPreset } from "@material-ui/styles";
import React from "react";
import { render, screen, fireEvent } from "../test-utils";
import Login from "./Login";

describe("tests Login component", () => {
  // beforeEach(() => {
  //   return render(<Login />)
  // })
  describe.skip("tests handleLogin", () => {
    it("should call handleSubmit on click", () => {
      const props = {
        form: {
          values: { email: "tset@example.com", password: "testpassword" },
        },
        loginRequest: jest.fn(),
      };
      render(<Login {...props} />, {
        initialState: {},
      });
      fireEvent.click(screen.getByRole("button"));
      expect(props.loginRequest).toHaveBeenCalledTimes(1)
    });
  });
});

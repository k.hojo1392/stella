import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// import styled from 'styled-components'
import { LoginForm } from "../components";
import { loginRequest } from "../reducks/auth/actions";
import { Box } from "@material-ui/core";
import { Padding } from "../components/UIkit/Padding";

// const StyledLoginContainer = styled(Box)`
//   padding: 20px;
//   ${({ isOpen }) => isOpen ? 'margin: 50px;' : ''}
// `;

const Login = (props) => {
  const { form } = props;

  const handleSubmit = (e) => {
    e.preventDefault();
    props.loginRequest(form.values);
  };

  return (
    <div>
      <h1>ログイン</h1>
      <Box display="flex" justifyContent="center">
        <Box
          border={1}
          borderColor="grey.500"
          borderRadius="borderRadius"
          minWidth={300}
        >
          {/* <StyledLoginContainer> */}
          {/* <div style={{padding: "20px"}}> */}
          <Padding top={20} bottom={20}>
            <LoginForm onSubmit={handleSubmit} />
          </Padding>
            {/* </div> */}
          {/* </StyledLoginContainer> */}
        </Box>
      </Box>
    </div>
  );
};

Login.propTypes = {
  form: PropTypes.object,
  loginRequest: PropTypes.func.isRequired,
};

export default connect(
  (state) => ({ form: state.form.login }),
  (dispatch) => {
    return { loginRequest: (values) => dispatch(loginRequest(values)) };
  }
)(Login);

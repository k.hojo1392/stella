import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { CollectionCard } from "../components/UIkit";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  card: {
    padding: theme.spacing(1),
  },
}));

const Collection = (props) => {
  const classes = useStyles();
  const { token, data } = props;

  return (
    <div>
      <h2>コレクション一覧</h2>
      {!token && (
        <p>
          コレクションを利用するには<Link to="/login">ログイン</Link>
          する必要があります。
        </p>
      )}
      {data.count}個のコレクション：
      <Grid container>
        {token &&
          data.count &&
          data.results.map((collection, i, results) => {
            return (
              <Grid
                item
                xs={6}
                p={1}
                key={collection.id}
                className={classes.card}
              >
                <CollectionCard col={collection} n={i} list={results} />
              </Grid>
            );
          })}
      </Grid>
    </div>
  );
};

Collection.propTypes = {
  token: PropTypes.string,
  data: PropTypes.shape({
    count: PropTypes.number,
    user: PropTypes.object,
    results: PropTypes.array,
  }),
};

export default connect((state) => ({
  token: state.auth.token,
  data: state.collection.data,
}))(Collection);

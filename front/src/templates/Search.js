import React from "react";
import { PropTypes } from "prop-types";
import ImmutablePropTypes from 'react-immutable-proptypes'
import { connect } from "react-redux";
import { changePageRequest, searchRequest } from "../reducks/search/actions";
import { SearchForm } from "../components";
import { BookCard, MyPagination } from "../components/UIkit";
import { Typography, Grid } from "@material-ui/core";
import styled from 'styled-components'

const Search = (props) => {
  const { form, result, number, page } = props;
  const from = (page - 1) * 10 + 1;
  const to = page * 10 <= number ? page * 10 : number;
  console.log(result)

  const handleSubmit = (e) => {
    e.preventDefault();
    props.searchRequest(form.values);
  };

  const handleChange = (e) => {
    console.log('clicked')
    props.changePageRequest(form.values, parseInt(e.target.textContent))
  }

  return (
    <div>
      <h1>NDC横断検索</h1>

      <SearchForm onSubmit={handleSubmit} />

      {number && (
        <Typography>
          {number}件の検索結果（{from}～{to}件目を表示）：
        </Typography>
      )}
      <Grid container>
        {result && result.size > 0 ?
          result.map((book, i) => {
            return (
              <Grid item xs={6} p={2} key={i}>
                <BookCard key={i} book={book} />
              </Grid>
            );
          }) : null}
      </Grid>

      {/* <div style={{ textAlign: "center", padding: 30 }}> */}
      <Wrapper>
        {number && (
          <MyPagination
            page={page}
            count={Math.ceil(parseInt(number, 10) / 10)}
            onChange={handleChange}
            variant="outlined"
            color="primary"
          />
        )}
      </Wrapper>
      {/* </div> */}
    </div>
  );
};

const Wrapper = styled.div`
  textAlign: center,
  padding: 30px
`

Search.defaultProps = {
  form: undefined,
  result: undefined,
  number: undefined,
  page: 1,
}

Search.propTypes = {
  form: PropTypes.object,
  result: ImmutablePropTypes.listOf(ImmutablePropTypes.mapOf(PropTypes.string)),
  number: PropTypes.string,
  page: PropTypes.number,
  searchRequest: PropTypes.func.isRequired,
  changePageRequest: PropTypes.func,
};

export default connect(
  (state) => ({
    form: state.form.search,
    result: state.search.result,
    number: state.search.number,
    page: state.search.page,
  }),
  (dispatch) => {
    return {
      searchRequest: values => dispatch(searchRequest(values)),
      changePageRequest: (values, page) => dispatch(changePageRequest(values, page))
    }
  }
)(Search);

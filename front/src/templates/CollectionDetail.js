import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { BookCard } from "../components/UIkit";
import { Typography, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  card: {
    padding: theme.spacing(1),
  },
}));

const CollectionDetail = (props) => {
  const classes = useStyles();
  const { detail } = props;
  const books = detail.books;

  return (
    <div>
      <h2>コレクション詳細</h2>
      <Typography variant="h2">{detail.name}</Typography>
      <Grid container>
        {books.map((item, i, books) => {
          const book = item.book;
          return (
            <Grid item xs={6} className={classes.card} key={item.id}>
              <BookCard
                book={book}
                n={i}
                list={books}
                collection_id={detail.id}
                item_id={item.id}
              />
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
};

CollectionDetail.propTypes = {
  detail: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    books: PropTypes.arrayOf(PropTypes.object),
  }),
};

export default connect((state) => ({ detail: state.detail.detail }))(
  CollectionDetail
);

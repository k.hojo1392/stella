import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { SignupForm } from "../components";
import { signupRequest } from "../reducks/auth/actions";
import { Box, Typography } from "@material-ui/core";
import { Padding } from "../components/UIkit/Padding";

const Signup = (props) => {
  const { form, done } = props;

  const handleSubmit = e => {
    e.preventDefault();
    signupRequest(form.values);
  }

  return (
    <div>
      <h2>アカウント登録</h2>
      <Box display="flex" justifyContent="center">
        <Box
          border={1}
          borderColor="grey.500"
          borderRadius="borderRadius"
          minWidth={300}
        >
          <Padding top={20} bottom={20}>
            <SignupForm
              onSubmit={handleSubmit}
            />
          </Padding>
        </Box>
      </Box>
      <Box display="flex" justifyContent="center">
        <Typography color="primary">{done}</Typography>
      </Box>
    </div>
  );
};

Signup.propTypes = {
  form: PropTypes.object,
  done: PropTypes.string,
  signupRequest: PropTypes.func,
};

export default connect(
  (state) => ({ form: state.form.signup, done: state.auth.done }),
  { signupRequest }
)(Signup);

import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from 'react-redux'
import { Link } from "react-router-dom";
import { verificationError, verificationRequest } from "../reducks/auth/actions";

class EmailVerify extends Component {
  async componentDidMount() {
    const key = this.props.match.params.key;
    if (key) {
      verificationRequest(key)
    } else {
      verificationError()
    };
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="custom-alert">
              {this.props.verified && (
                <div className="alert alert-success" role="alert">
                  <p>
                    登録が完了しました。<Link to="/login">ログイン</Link>
                  </p>
                </div>
              )}
              {!this.props.verified && (
                <div className="alert alert-danger" role="alert">
                  <p>
                    登録がすでに完了しているか、リンクの期限が切れている可能性があります。
                  </p>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

EmailVerify.propTypes = {
  match: PropTypes.object,
  verified: PropTypes.bool,
  verificationRequest: PropTypes.func,
  verificationError: PropTypes.func,
};

export default connect(
  state => state.auth.verified,
  {verificationRequest, verificationError}
)(EmailVerify);

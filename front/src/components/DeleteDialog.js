import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Dialog,
  DialogTitle,
  Button,
  DialogActions,
  DialogContentText,
  Typography,
} from "@material-ui/core";
import { handleDelete } from "../reducks/collection/operations";

const DeleteDialog = (props) => {
  // nは配列のindex,targetidはDB上のidなので別物。
  // DBのデータ操作にはtargetid、表示の操作にはnが必要
  const { token, open, onClose, targetId, targetName, n, list } = props;

  let targetType = "";
  if (!targetId.item) {
    //呼び出し元がCollectionCard
    targetType = "コレクション";
  } else {
    // 呼び出し元がBookCard
    targetType = "アイテム";
  }

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{targetType}を削除しますか？</DialogTitle>
      <DialogContentText>
        <Typography>タイトル：{targetName}</Typography>
        <Typography color="secondary" variant="caption">
          この操作は元に戻せません
        </Typography>
      </DialogContentText>
      <DialogActions>
        <Button
          variant="outlined"
          color="secondary"
          onClick={handleDelete(token, targetId, n, list)}
        >
          はい
        </Button>
        <Button variant="contained" color="primary" onClick={() => onClose(!open)}>
          いいえ
        </Button>
      </DialogActions>
    </Dialog>
  );
};

// DeleteDialog.defaultProps = {
//   open: false,
//   targetId: undefined,
//   targetName: undefined,
//   n: undefined,
//   list: undefined,
// }

DeleteDialog.propTypes = {
  token: PropTypes.string,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  targetId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.objectOf(PropTypes.number),
  ]),
  targetName: PropTypes.string,
  n: PropTypes.number,
  list: PropTypes.object,
  handleDelete: PropTypes.func,
};

export default connect((state) => ({ token: state.auth.token }), {
  handleDelete,
})(DeleteDialog);

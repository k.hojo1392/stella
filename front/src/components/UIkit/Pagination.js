import React from "react";
import PropTypes from "prop-types";
import { Pagination } from "@material-ui/lab";
import { withStyles } from "@material-ui/styles";

const styles = {
  root: {
    flexGrow: 1,
    display: "inline-block",
  },
};

const MyPagination = (props) => {
  const { classes, ...rest } = props;
  return <Pagination className={classes.root} {...rest} />;
};

MyPagination.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.object,
  }),
};

export default withStyles(styles)(MyPagination);

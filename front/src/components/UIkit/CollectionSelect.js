import React from "react";
import PropTypes from "prop-types";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Divider,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  formControl: {
    minWidth: 200,
  },
}));

const CollectionSelect = (props) => {
  const { onChange, results, col } = props;
  const classes = useStyles();

  return (
    <FormControl className={classes.formControl}>
      <InputLabel>追加先コレクション</InputLabel>
      <Select value={col} onChange={onChange}>
        {results &&
          results.map((collection) => {
            return (
              <MenuItem key={collection.id} value={collection}>
                {collection.name}
              </MenuItem>
            );
          })}
        <Divider />
        <MenuItem key="new" value="new">
          新規作成
        </MenuItem>
      </Select>
    </FormControl>
  );
};

CollectionSelect.propTypes = {
  onChange: PropTypes.func,
  results: PropTypes.arrayOf(PropTypes.object),
  col: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

export default CollectionSelect;

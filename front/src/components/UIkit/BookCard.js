import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Card,
  CardActions,
  CardContent,
  Typography,
  IconButton,
} from "@material-ui/core";
import LibraryAddIcon from "@material-ui/icons/LibraryAdd"
import DeleteIcon from '@material-ui/icons/Delete';
import DeleteDialog from "../DeleteDialog";
import { handleDelete } from "../../reducks/collection/operations";
import AddDialogSearch from "../AddDialogSearch";

class BookCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAddOpen: false,
      isDeleteOpen: false,
    };
    this.toggleAdd = this.toggleAdd.bind(this);
    this.toggleDelete = this.toggleDelete.bind(this);
  }

  toggleAdd() {
    this.setState({ isAddOpen: !this.isAddOpen });
  };

  toggleDelete() {
    this.setState({ isDeleteOpen: !this.isDeleteOpen });
  };

  render() {
    const { pathname, book, n, list, collection_id, item_id } = this.props;
    return (
      <Card>
        <CardContent>
          <Typography variant="h4">{book.get('title')}</Typography>
          <Typography>{book.get('creator')}</Typography>
          <Typography>
            {book.get('publisher')}({book.get('issued')})
          </Typography>

          <AddDialogSearch
            open={this.state.isAddOpen}
            onClose={this.toggleAdd}
            book={book}
          />
          <DeleteDialog
            open={this.state.isDeleteOpen}
            onClose={this.toggleDelete}
            targetId={
              collection_id ? { col: collection_id, item: item_id } : ""
            }
            targetName={book.title}
            n={n}
            list={list}
          />
        </CardContent>
        <CardActions>
          <IconButton
            onClick={() => this.toggleAdd(!this.isAddOpen)}
            disabled={
              pathname === "/collection/" + collection_id ? true : false
            }
          >
            <LibraryAddIcon />
          </IconButton>
          <IconButton
            onClick={() => this.toggleDelete(!this.isDeleteOpen)}
            disabled={
              pathname === "/search" ? true : false
            }
          >
            <DeleteIcon />
          </IconButton>
        </CardActions>
      </Card>
    );
  }
}

BookCard.propTypes = {
  pathname: PropTypes.string,
  book: PropTypes.any,
  // book: PropTypes.shape({
  //   title: PropTypes.string,
  //   creator: PropTypes.string,
  //   publisher: PropTypes.string,
  //   issued: PropTypes.string,
  // }),
  n: PropTypes.number,
  list: PropTypes.array,
  collection_id: PropTypes.oneOfType([
    PropTypes.objectOf(PropTypes.number),
    PropTypes.string,
  ]),
  item_id: PropTypes.number,
};
export default connect(
  (state) => ({ pathname: state.router.location.pathname }),
  { handleDelete }
)(BookCard);

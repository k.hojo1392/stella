import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link as RouterLink } from "react-router-dom";
import Link from "@material-ui/core/Link";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import LibraryAddIcon from "@material-ui/icons/LibraryAdd";
import DeleteIcon from "@material-ui/icons/Delete";
import { showDetail } from "../../reducks/collection/actions";
import DeleteDialog from "../DeleteDialog";
import AddDialog from "../AddDialogCollection";

const CollectionCard = (props) => {
  const { col, n, list, showDetail } = props;
  const [isAddOpen, toggleAdd] = useState(false);
  const [isDeleteOpen, toggleDelete] = useState(false);
  const created_at = new Date(col.created_at);
  const updated_at = new Date(col.updated_at);

  return (
    <Card>
      <CardContent>
        <Typography variant="h4">
          <Link
            component={RouterLink}
            to={{ pathname: "/collection/" + col.id }}
            color="inherit"
          >
            <Button onClick={() => showDetail(col)}>{col.name}</Button>
          </Link>
        </Typography>
        <Typography>{col.books.length}冊の本</Typography>
        <Typography>
          作成日時：{created_at.toLocaleDateString("ja-JP")}
          {created_at.toLocaleTimeString("ja-JP")}
          <br />
          更新日時：{updated_at.toLocaleDateString("ja-JP")}
          {updated_at.toLocaleTimeString("ja-JP")}
        </Typography>

        <AddDialog open={isAddOpen} onClose={toggleAdd} collection={col} />

        <DeleteDialog
          key={col.id}
          open={isDeleteOpen}
          onClose={toggleDelete}
          n={n}
          list={list}
          targetId={col.id}
          targetName={col.name}
        />
      </CardContent>

      <CardActions>
        <IconButton onClick={() => toggleAdd(!isAddOpen)}>
          <LibraryAddIcon />
        </IconButton>
        <IconButton onClick={() => toggleDelete(!isDeleteOpen)}>
          <DeleteIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};

CollectionCard.propTypes = {
  col: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    created_at: PropTypes.string,
    updated_at: PropTypes.string,
    books: PropTypes.arrayOf(PropTypes.object),
  }),
  n: PropTypes.number,
  list: PropTypes.array,
  showDetail: PropTypes.func,
};

export default connect((state) => state, { showDetail })(CollectionCard);

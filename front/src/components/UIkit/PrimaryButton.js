import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";

const PrimaryButton = (props) => {
  const { type, label } = props;
  return (
    <Button variant="contained" color="primary" type={type}>
      {label}
    </Button>
  );
};

PrimaryButton.propTypes = {
  type: PropTypes.string,
  label: PropTypes.string,
};

export default PrimaryButton;

import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { Box } from "@material-ui/core";
import { PrimaryButton, renderTextInput } from "./UIkit";

let SearchForm = (props) => {
  const { onSubmit } = props;
  const ndc = [
    "総記",
    "哲学",
    "歴史",
    "社会科学",
    "自然科学",
    "技術",
    "産業",
    "芸術",
    "言語",
    "文学",
  ];

  return (
    <div>
      <form onSubmit={onSubmit}>
        <div style={{ width: "100%" }}>
          <Box
            display="flex"
            flexDirection="row"
            alignItems="center"
            justifyContent="center"
            flexWrap="nowrap"
          >
            <Field
              name="title"
              component={renderTextInput}
              type="text"
              placeholder="キーワード"
            />
            の
            <Field name="ndc" component="select">
              <option key="default" value="default">
                （分類を選択）
              </option>
              {ndc.map((v, i) => {
                return (
                  <option key={i} value={i}>
                    {v}
                  </option>
                );
              })}
            </Field>
            的側面
            <Box p={2}>
              <PrimaryButton type="submit" label="検索" />
            </Box>
          </Box>
        </div>
      </form>
    </div>
  );
};

SearchForm.propTypes = {
  onSubmit: PropTypes.func,
};

export default SearchForm = reduxForm({
  form: "search",
})(SearchForm);

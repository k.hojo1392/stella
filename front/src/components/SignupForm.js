import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { Grid } from "@material-ui/core";
import { PrimaryButton, renderTextInput } from "./UIkit";

let SignupForm = (props) => {
  const { onSubmit } = props;
  return (
    <form onSubmit={onSubmit}>
      <Grid container direction="column" alignItems="center" spacing={3}>
        <Grid item>
          <Field
            name="email"
            label="メールアドレス"
            component={renderTextInput}
            type="email"
            required={true}
            fullWidth={false}
          />
        </Grid>
        <Grid item>
          <Field
            name="username"
            label="ユーザー名"
            component={renderTextInput}
            type="text"
            required={true}
            fullWidth={false}
          />
        </Grid>
        <Grid item>
          <Field
            name="password1"
            label="パスワード"
            component={renderTextInput}
            type="password"
            required={true}
            fullWidth={false}
          />
        </Grid>
        <Grid item>
          <Field
            name="password2"
            label="パスワード（確認用）"
            component={renderTextInput}
            type="password"
            required={true}
            fullWidth={false}
          />
        </Grid>
        <Grid item>
          <PrimaryButton label="送信" type="submit" />
        </Grid>
      </Grid>
    </form>
  );
};

SignupForm.propTypes = {
  onSubmit: PropTypes.func,
};

export default SignupForm = reduxForm({ form: "signup" })(SignupForm);

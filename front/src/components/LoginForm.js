import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { Grid } from "@material-ui/core";
import { PrimaryButton, renderTextInput } from "./UIkit";

let LoginForm = (props) => {
  const { onSubmit } = props;
  return (
    <>
      <form onSubmit={onSubmit}>
        <Grid container direction="column" alignItems="center" spacing={3}>
          <Grid item>
            <Field
              name="email"
              label="メールアドレス"
              component={renderTextInput}
              type="email"
              required={true}
              fullWidth={false}
            />
          </Grid>
          <Grid item>
            <Field
              name="password"
              label="パスワード"
              component={renderTextInput}
              type="password"
              required={true}
              fullWidth={false}
            />
          </Grid>
          <Grid item>
            <PrimaryButton label="ログイン" type="submit" />
          </Grid>
        </Grid>
      </form>

      {/* <Box display="flex" justifyContent="center">
        {error && <Typography color="secondary">メールアドレスまたはパスワードが違います。</Typography>}
      </Box> */}
    </>
  );
};

LoginForm.propTypes = {
  onSubmit: PropTypes.func,
};

export default LoginForm = reduxForm({ form: "login" })(LoginForm);

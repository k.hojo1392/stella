import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { handleAdd_Search } from "../reducks/collection/operations";
import {
  Dialog,
  DialogTitle,
  DialogContentText,
  DialogActions,
  Button,
  DialogContent,
  Box,
  TextField,
} from "@material-ui/core";
import CollectionSelect from "./UIkit/CollectionSelect";

class AddDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      col: "",
      collectionName: "",
    };
    this.handleChangeCollection = this.handleChangeCollection.bind(this);
  }

  handleChangeCollection(e) {
    this.setState({ col: e.target.value });
  };

  render() {
    const { token, open, onClose, book } = this.props;
    return (
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>アイテムを追加しますか？</DialogTitle>
        <DialogContent>
          <Box>
            <CollectionSelect
              onChange={(e) => this.handleChangeCollection(e)}
              // results={results}
              col={this.state.col}
            />
          </Box>

          <Box>
            {this.state.col === "new" && (
              <TextField
                placeholder="コレクション名"
                value={this.collectionName}
                onChange={(e) =>
                  this.setState({ collectionName: e.target.value })
                }
              ></TextField>
            )}
          </Box>
          <Box component="div" style={{ marginTop: 10 }}>
            <DialogContentText>{book.title}</DialogContentText>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleAdd_Search(
                token,
                book,
                this.state.col,
                // results,
                this.state.collectionName
              );
              this.setState({ col: "" });
              onClose(!open);
            }}
          >
            追加
          </Button>
          <Button variant="outlined" onClick={() => onClose(!open)}>
            キャンセル
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

AddDialog.defaultProps = {
  results: []
}

AddDialog.propTypes = {
  token: PropTypes.string,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  results: PropTypes.arrayOf(PropTypes.object),
  book: PropTypes.objectOf(PropTypes.string),
};

export default connect((state) => ({
  token: state.auth.token,
  // results: state.collection.data.results,
}))(AddDialog);

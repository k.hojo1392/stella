import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  Button,
  DialogContent,
  Box,
  Typography,
} from "@material-ui/core";
import BookForm from "./UIkit/BookForm";

class AddDialogCollection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      books: [{ book: { title: "", creator: "", publisher: "", issued: "" } }],
    };
    this.handleChangeBooks = this.handleChangeBooks.bind(this);
    this.clearBooks = this.clearBooks.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleChangeBooks = (e) => {
    const target = e.target;
    const name = target.name;
    const value = target.value;
    this.setState({ books: { ...this.state.books, [name]: value } });
  };

  clearBooks = () => {
    this.setState({
      books: [{ book: { title: "", creator: "", publisher: "", issued: "" } }],
    });
  };

  handleClose = () => {
    this.clearBooks();
    this.props.onClose(!this.props.open);
  };

  render() {
    const { token, open, onClose, collection } = this.props;
    return (
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>アイテムを追加しますか？</DialogTitle>
        <DialogContent>
          <Typography>追加先コレクション：</Typography>
          <Typography variant="h6">{collection.name}</Typography>

          <Box>
            <BookForm
              onChange={this.handleChangeBooks}
              books={this.state.books}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              this.handleAddCollection(token, collection, this.state.books);
              this.clearBooks();
              onClose(!open);
            }}
          >
            追加
          </Button>
          <Button variant="outlined" onClick={this.handleClose}>
            キャンセル
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

AddDialogCollection.propTypes = {
  token: PropTypes.string,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  collection: PropTypes.shape({
    name: PropTypes.string,
    books: PropTypes.arrayOf(PropTypes.object),
  }),
};

export default connect((state) => ({ token: state.auth.token }))(
  AddDialogCollection
);

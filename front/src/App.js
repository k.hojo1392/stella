import React from "react";
// import PropTypes from 'prop-types';
// import {connect} from 'react-redux';
import Router from "./Router";
// import {initCollections} from './reducks/collection/operations'

const App = () => {
  return (
    <main>
      <Router />
    </main>
  );
  // }
};

// App.propTypes = {
//   token: PropTypes.string,
// }

// export default connect(
//   state => ({token: state.auth.token}),
//   {initCollections}
// )(App);
export default App;
